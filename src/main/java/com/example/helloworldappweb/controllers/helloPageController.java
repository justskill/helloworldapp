package com.example.helloworldappweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class helloPageController {

    @RequestMapping("/hello")
    public String testPage() {
        return "hello.html";
    }
}
