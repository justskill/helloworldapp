package com.example.helloworldappweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class testPageController {

    @RequestMapping("/test")
    public String testPage() {
        return "test.html";
    }
}
